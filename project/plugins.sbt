addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")

enablePlugins(BuildInfoPlugin)
val playGrpcV = "0.8.1"
buildInfoKeys := Seq[BuildInfoKey]("playGrpcVersion" -> playGrpcV)
buildInfoPackage := "play.scala.grpc"


addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.1")

// #grpc_sbt_plugin
// project/plugins.sbt
addSbtPlugin("com.lightbend.akka.grpc" %% "sbt-akka-grpc" % "0.7.3")
addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.4")
libraryDependencies += "com.lightbend.play" %% "play-grpc-generators" % playGrpcV
// #grpc_sbt_plugin
