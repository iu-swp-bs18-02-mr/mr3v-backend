import scala.collection.JavaConverters.asScalaSet
import com.google.inject.AbstractModule
import com.google.inject.multibindings.Multibinder
import play.grpc.internal.PlayRouter
import org.reflections.Reflections

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
class Module extends AbstractModule {
  override def configure(): Unit = {
    val grpcRouterBinder: Multibinder[PlayRouter] = Multibinder.newSetBinder(binder(), classOf[PlayRouter])
    val routersGrpcReflections = new Reflections("routers.grpc")
    asScalaSet(routersGrpcReflections.getSubTypesOf(classOf[PlayRouter]))
      .filter { _.getAnnotations.map { _.annotationType().getName }.contains("routers.grpc.annotations.GRPCRouter") }
      .map { grpcRouterBinder.addBinding().to(_) }

    // Service, which will run the code needed to initialize the application
    bind(classOf[StartUpService]).asEagerSingleton()
  }
}
