package controllers.dashboard.model.management

import javax.inject.Inject
import java.io.File

import akka.stream.scaladsl.{ FileIO, Source }
import akka.util.ByteString
import play.api.Configuration
import play.api.http.HttpEntity
import play.api.libs.json.Json
import play.api.libs.streams.Accumulator
import play.api.mvc.{ AbstractController, BodyParser, ControllerComponents, ResponseHeader, Result }
import play.api.libs.Files.TemporaryFile

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.Try
import cats.implicits._

import services.ModelConverterService

class ModelFileController @Inject() (
  cc: ControllerComponents,
  config: Configuration,
  modelConverterService: ModelConverterService
//  implicit val materializer: akka.stream.Materializer
) extends AbstractController(cc) {
  private implicit def ec: ExecutionContext = cc.executionContext

  private val uploadDirectory: String = config.getOptional[String]("fileUploadDirectory").getOrElse("uploads/")
  private val convertibleModelExtensions = modelConverterService.convertibleModelExtensions
  private val modelFileExtension = ".glb"
  private val allowedModelExtensions = convertibleModelExtensions + modelFileExtension
  private val fileNameGlbPattern = "^(.*)\\.glb$".r

  type UploadedModel = Either[File, (TemporaryFile, File, String)]

  // TODO: maybe this parser can return a parsed gltf model
  private def newModelParser: BodyParser[UploadedModel] = BodyParser { request =>
    val validatedFileHandle = request.headers.get("Filename")
      .toRight(BadRequest("Filename header must be specified"))
      .flatMap { fullFilename =>
        val fileName =
          Try { fullFilename.substring(0, fullFilename.lastIndexOf('.')) }
        val fileExtension =
          Try { fullFilename.substring(fullFilename.lastIndexOf('.'), fullFilename.length) }

        for {
          fileExt <- fileName.map2(fileExtension) { (_, fileExt) => fileExt }.toOption
            .toRight(BadRequest("File name and extension must be separated with '.'"))
          _ <- fileName.filter(_.nonEmpty).toOption
            .toRight(BadRequest("The file name must nor be empty"))
          _ <- fileExtension.filter(allowedModelExtensions.contains).toOption
            .toRight(BadRequest(
              "The uploading file must have either one of the following extensions:\n" +
                allowedModelExtensions.mkString(" | ")
            ))
          file <- Try { new File(uploadDirectory + fullFilename) }.toOption
            .toRight(InternalServerError("Failed to create a file handle"))
          _ <- Some(file)
            .filter(!_.exists)
            .toRight(BadRequest("The file with such name has already been uploaded"))
        } yield {
          (file, fileExt)
        }
      }

    validatedFileHandle match {
      case Right((file, fileExtension)) if fileExtension == modelFileExtension =>
        parse.file(file)(request).map { _.map{ Left(_) } }
      case Right((file, fileExtension)) =>
        parse.temporaryFile(request).map { _.map { Right(_, file, fileExtension) } }
      case Left(res) =>
        Accumulator.done(Left(res))
    }
  }

  /**
   * Stores a 3D model file, that is passed in the body on the server with the name
   * specified by a `Filename` header. For the list of available formats, refer to
   * `allowedModelExtensions`
   */
  def saveModel = Action.async(newModelParser) { request =>
      request.body match {
        case Left(_) => Future.successful(Ok)
        case Right((tempFile, fileToSave, fileExtension)) =>
          Future.successful(NotImplemented)
    }
  }

  /**
   * Given a filename (the list of all uploaded files can be retrieved from
   * `getFileList` method) returns the file contents in the body as a stream.
   * The file returned is a glTF model.
   *
   * @param filename name of the file to retrieve without the extension.
   * @return File in the body, if the file exists, 404 otherwise.
   */
  def getModel(filename: String) = Action.async {
    Future {
      val file = new File(s"${uploadDirectory}${filename}${modelFileExtension}")
      val source: Source[ByteString, _] = FileIO.fromPath(file.toPath)

      if (file.exists())
        Result(
          header = ResponseHeader(OK, Map.empty),
          body = HttpEntity.Streamed(source, Some(file.length), Some(BINARY))
        )
      else
        NotFound("The file with the given name does not exist")
    }
  }

  /**
   * Get the list of all loaded models.
   *
   * @return Json document, with the key `"filenames"`, value of which is a list
   *         of names of the uploaded models without the extension.
   */
  def getFileList = Action.async {
    Future {
      Ok(Json.obj(
        "filenames" -> Json.toJson(
          (new File(uploadDirectory).list()).map {
            case fileNameGlbPattern(filename) => filename
            case _ => ""
          }.filter { _.nonEmpty }
        )
      ))
    }
  }
}
