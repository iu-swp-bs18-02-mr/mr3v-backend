import java.io.File

import javax.inject.{ Inject, Singleton }
import play.api.Configuration

@Singleton
/**
 * Service, which contains code needed to initialize application
 * */
class StartUpService @Inject() (config: Configuration) {
  // Create the folder to store uploads
  private val uploadDirectoryPath: String = config.getOptional[String]("fileUploadDirectory").getOrElse("uploads/")
  val uploadDirectory = new File(uploadDirectoryPath)
  if (!uploadDirectory.exists())
    if (!uploadDirectory.mkdirs())
      throw new IllegalStateException(s"Not able to create the directory for uploads: $uploadDirectoryPath")
}
